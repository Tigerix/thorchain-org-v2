# syntax=docker/dockerfile:1
FROM node:14.15.4 as base

WORKDIR /code

# COPY ["package.json", "package-lock.json", "./"]

FROM base as prod
RUN npm i express
RUN npm i axios
RUN npm i cors
COPY api/ api/
COPY utils/ utils/
CMD [ "node", "api/server.js"]