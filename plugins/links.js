const links = {
    navBar: {
        technology: "technology",
        rune: "rune",
        decentralized: "decentralized",
        ecosystem: "ecosystem",
        community: "community",
        roles: "roles",
        economics: "economics",
        bepswap: "https://chaosnet.bepswap.com/",
        docs: "https://docs.thorchain.org/",
        blog: "https://thorchain.medium.com/",
    },
    shapeshift: "http://shapeshift.app.link/wDRGTdULMcb",
    asgardex: "https://testnet.asgard.exchange",
    blockExplorer: "https://viewblock.io/thorchain",    
    telegram: "https://t.me/thorchain_org",
    telegramDev: "https://t.me/thorchain_dev",
    gitlab: "https://gitlab.com/thorchain",
    twitter: "https://twitter.com/thorchain_org",
    discord: " https://discord.gg/WCDWfgq",
    activeAsgardVault: "dynamic!", // will set at nuxtServerInit() in store
    activeAsgardVaults: "https://viewblock.io/thorchain/vaults",
    midgardApiPage: "https://docs.thorchain.org/developers/midgard-api",
    thorchainTSSWhitepaper: "https://github.com/thorchain/Resources/blob/master/Whitepapers/THORChain-TSS-Paper-June2020.pdf",
    thorchainTSSBenchmark: "https://github.com/thorchain/Resources/blob/master/Whitepapers/THORChain-TSS-Benchmark-July2020.pdf",
    kudelskiTSSAudit: "https://github.com/thorchain/Resources/blob/master/Audits/THORChain-Kudelski-TSS-Audit-June2020.pdf",
    thorchainTransactions: "https://viewblock.io/thorchain/txs",
    coingeckoMarket: "https://www.coingecko.com/en/coins/thorchain#markets",
    exploreTransactions: "https://viewblock.io/thorchain/txs",
    emissionSchedule: "https://docs.google.com/spreadsheets/d/1e5A7TaV6CZtdVqlOSuXSSY7UYiRW9yzd1ST6QTZNqLw/edit#gid=918223980",
    blockRewardCal: "https://docs.thorchain.org/how-it-works/emission-schedule",
    simulateValue: "https://www.thorchain.community",
    transactionMemos: "https://docs.thorchain.org/developers/transaction-memos",
    guideMe: "https://medium.com/thorchain/an-in-depth-guide-to-thorchains-liquidity-pools-c4ea7829e1bf",
    provideLiquidity: "https://medium.com/thorchain/an-in-depth-guide-to-thorchains-liquidity-pools-c4ea7829e1bf",
    thornodeBonds: "https://viewblock.io/thorchain/validators",
    supportingChains: "https://twitter.com/thorchain_org/status/1304211812049534976",
    thornodeOps: "https://docs.thorchain.org/thornodes/overview",
    scrollTo: {
        "technology": ['how-does-it-work', 'how-does-it-compare', 'has-it-been-audited', 'how-active-is-it'],
        "rune": ["what-does-it-do", "how-many-will-exist", "what-influences-it", "where-do-I-get-it", "how-do-I-use-it"]
    },
    arbitraging: "https://twitter.com/mehowbrains/status/1302820452415438848?s=20",
    swapping: "https://chaosnet.bepswap.com",
    incentivePendulum: "https://docs.thorchain.org/how-it-works/incentive-pendulum",
    feeInfo: "https://docs.thorchain.org/how-it-works/fees",
    continuousLiqPools: "https://docs.thorchain.org/how-it-works/continuous-liquidity-pools",
    priceOracle: "https://docs.thorchain.org/how-it-works/prices",
    governance: "https://docs.thorchain.org/how-it-works/governance",
    thorExplainerVideo: "https://twitter.com/mehowbrains/status/1291018249698791426?s=20",
    runeBridge: "https://runebridge.org",
    sushiSwapPools: "https://runebridge.org/pools",
    nodeOperators: "https://docs.thorchain.org/roles/node-operators",
    activeNodes200to300: "https://twitter.com/thorchain_org/status/1348126879874617351?s=20",
    nodesChurnNext: "https://defi.delphidigital.io/thorchain/chaosnet/network",
    vaults: "https://viewblock.io/thorchain/vaults",
    pools: "https://viewblock.io/thorchain/pools",
    nodes: "https://viewblock.io/thorchain/validators",
    thorStatistics: "https://thorchain.net/",
    connectToThorchain: "https://docs.thorchain.org/developers/connecting-to-thorchain",
    getFunded: "https://twitter.com/thorchain_org/status/1315850914092052480?s=20",
    viewStat: 'https://thorchain.net',
    websiteRoadMap: "https://medium.com/@thorchaincommunity/new-thorchain-org-v0-1-62b996980c5",
    feedbackChannel: "https://t.me/thorchainfeedback"
}

export default ($context, inject, ) => {
   
    /* assign function to dynamic routes */
    links.activeAsgardVault = $context.$services.getActiveAsgardVaultLink

    inject('links', (query) => {
        return byString(links, query)
    })
}

function byString(o, s) { // Reference: https://stackoverflow.com/questions/6491463/accessing-nested-javascript-objects-and-arrays-by-string-path
    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    s = s.replace(/^\./, '');           // strip a leading dot
    var a = s.split('.');
    for (var i = 0, n = a.length; i < n; ++i) {
        var k = a[i];
        if (k in o) {
            o = o[k];
        } else {
            return;
        }
    }
    return o;
}