import Vue from 'vue'
import VueGtag from 'vue-gtag'

var trackingId = null
if(process.env.isProduction){
    trackingId = 'G-QDG7Z69FRZ'     
} else {
    trackingId = 'G-25M8J6EHXX'
}
console.log("VueGtag --> " + (process.isProduction ? "Production" : "Dev"))
console.log("isProduction: "+process.env.isProduction)
console.log("VERCEL: "+ process.env.VERCEL)
Vue.use(VueGtag, {
  config: { id: trackingId },
  globalObjectName: 'EventCollector'
})