const replace = require('replace-in-file');
var matchCounter = 0;
const options = {
    // files: '../assets/styles/main.scss',
    // files: '../components/base/*',
    // files: '../pages/technology/*',
    // files: '../pages/rune/*',
    files: '../pages/index.vue',
    // files: '../components/page-components/CountDownTimer.vue',
    // files: './CoingeckoTable.vue',
    // files: '../mobile_layout/components/**/*',
    from: /(font-size|margin.*|line-height|padding.*|width|height|top|left|right|bottom|border-radius|border|letter-spacing)\s*:\s*((-?(\d+|\d+\.\d*)px\s?)|(0\s+))+/g,
    // from: /(border|letter-spacing)\s*:\s*((-?(\d+|\d+\.\d*)px\s?)|(0\s+))+/g,

    to: (match) => {
        var [attrName, sizes] = match.split(':')
        sizes = sizes.split(/0\s+/).join('0px ')
        var numbers = sizes.split('px')
        var ret = ""
        for(var n of numbers){
            if(isNaN(parseFloat(n))) continue;
            var remVal = parseInt(n)/16
            ret += remVal + "rem "
        }
        console.log(matchCounter+". "+ match+" \t-> "+attrName+": "+ret)
        matchCounter++
        return attrName+": "+ret
    },
    dry: false
};
replace(options)
  .then(results => {
    console.log('Replacement results:', results);
    console.log(results
        .filter(result => result.hasChanged)
        .map(result => result.file))
  })
  .catch(error => {
    console.error('Error occurred:', error);
  });