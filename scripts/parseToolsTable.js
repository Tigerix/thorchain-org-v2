// Reading the file using default 
// fs npm package  
const fs = require("fs"); 
const { exit } = require("process");
const { Console } = require("console");
csv = fs.readFileSync("Master Links - Tools Table.csv") 
  
// Convert the data to String and 
// split it in an array 
var array = csv.toString().split("\r"); 
  
// All the rows of the CSV will be  
// converted to JSON objects which  
// will be added to result in an array 
let result = []; 
  
// The array[0] contains all the  
// header columns so we store them  
// in headers array 
let headers = array[0].split(/,\s*/) 
console.log(headers)
// Since headers are separated, we  
// need to traverse remaining n-1 rows.
var json = {
    locale_file_tools_table_content:{},
    links_plugin_tools_table_links: {
        tools_table: {}
    }
}

for (let i = 2; i < array.length; i++) { 
    // replace all the \n with nothing
    array[i] = array[i].split(['\n']).join()
    console.log(array[i])
    var attrs = array[i].split(/,\s*/) 
    var label = attrs[6]
    var tableContentObject = {
        category: attrs[1],
        name: attrs[2],
        utility: attrs[7],
        repo: attrs[8] == "Leave Empty"? "": attrs[8],
        owner: attrs[13]== "Leave Empty"? "": attrs[13],
    }
    json.locale_file_tools_table_content[label] = tableContentObject

    var linksPluginObject = {
        tool_link: {
            link: attrs[3],
            action: attrs[4],
            category: attrs[5],
            label: label
        },
        repo_url: attrs[9] == "No URL" || attrs[9] == "Private" ? null : {
            link: attrs[9],
            action: attrs[10],
            category: attrs[11],
            label: attrs[12]
        },
        telegram_url: attrs[14] == "Don't show"? null: {
            link: attrs[14],
            action: attrs[15],
            category: attrs[16],
            label: attrs[17]
        },
        github_url: attrs[18] == "Don't show"? null: {
            link: attrs[18],
            action: attrs[19],
            category: attrs[20],
            label: attrs[21]
        },
        gitlab_url: attrs[22] == "Don't show"? null: {
            link: attrs[22],
            action: attrs[23],
            category: attrs[24],
            label: attrs[25]
        },
        twitter_url: attrs[26] == "Don't show"? null: {
            link: attrs[26],
            action: attrs[27],
            category: attrs[28],
            label: attrs[29]
        }
    }
    console.log(linksPluginObject)
    json.links_plugin_tools_table_links.tools_table[label] = linksPluginObject
        
}
fs.writeFileSync('toolsTableOutput.json', JSON.stringify(json, null, 2))