module.exports = {
  numberWithCommas(x) {
    if (x)
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    else 
      return undefined
  },
  nFormatter(num, digits = 1) {
    var si = [
      { value: 1, symbol: "" },
      { value: 1e3, symbol: "k" },
      { value: 1e6, symbol: "M" },
      { value: 1e9, symbol: "G" },
      { value: 1e12, symbol: "T" },
      { value: 1e15, symbol: "P" },
      { value: 1e18, symbol: "E" },
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
      if (num >= si[i].value) {
        break;
      }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
  },
  curFormatter(num, digits = 1) {
    var si = [
      { value: 1, symbol: "" },
      { value: 1e3, symbol: "k" },
      { value: 1e6, symbol: "M" },
      { value: 1e9, symbol: "B" },
      { value: 1e12, symbol: "T" },
      { value: 1e15, symbol: "P" },
      { value: 1e18, symbol: "E" },
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
      if (num >= si[i].value) {
        break;
      }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
  },
  timeFormatter(seconds) {
    var minutes = Math.floor(seconds / 60);

    var remMins = minutes % 60;

    var hours = Math.floor(seconds / (60 * 60));

    var remHrs = hours % 24;

    var days = Math.floor(seconds / (24 * 60 * 60));

    let timeStr = ""
    if (days == 1) {
      timeStr += "1 Day"
    }
    else if (days > 1) {
      timeStr += `${days} Days `
    }
    
    if (remHrs > 0) {
      timeStr += ` ${remHrs} Hrs `
    }
    
    if (remMins > 0) {
      timeStr += ` ${remMins} Mins`
    }

    return timeStr
  },
  timeConversion(millisec) {
    var seconds = (millisec / 1000).toFixed(0);

    var minutes = (millisec / (1000 * 60)).toFixed(0);

    var hours = (millisec / (1000 * 60 * 60)).toFixed(0);

    var days = (millisec / (1000 * 60 * 60 * 24)).toFixed(0);

    if (seconds < 60) {
      return seconds + " Sec";
    } else if (minutes < 60) {
      return minutes + " Min";
    } else if (hours < 24) {
      return hours + " Hrs";
    } else {
      return days + " Days"
    }
  },
  sleep(millisec) {
    return new Promise((resolve) => {
      setTimeout(resolve, millisec);
    });
  },
};
