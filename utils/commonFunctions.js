var utils = require("./index.js");
const { numberWithCommas } = require("./index.js");

module.exports = {
    async fetchHomePageData($services) {
        var runePrice = await $services.getRunePrice()
        var netValuesMCCN = await $services.getThorNetValuesMCCN()
        var { lpAPY } = parseNetValuesGetAPY(netValuesMCCN)

        var { totalCapital } = parseNetValuesGetCapitals(netValuesMCCN, runePrice)
        var coingecko = await $services.getCoingeckoData()
        var runeLocked = parseNetValuesGetRuneLocked(totalCapital, coingecko)
        var lastBlock = await $services.getLastBlock()
        var nodesLocation = await $services.getNodesLocation()

        var MCCN = {
            ...parseNetValuesGetCapitals(netValuesMCCN, runePrice),
            ...parseNetValuesGetStats(netValuesMCCN, lastBlock, runePrice),
            ...parseNetValuesGetAPY(netValuesMCCN),
        }
        MCCN['runeLocked'] = parseNetValuesGetRuneLocked(MCCN.totalCapital, coingecko)

        return {
            lpAPY,
            runeLocked,
            MCCN,
            nodesLocation
        }
    },
    async fetchTechnologyViewData($services) {
        var minimumBond = await $services.getMinimumBond()
        var runePrice = await $services.getRunePrice()

        if (runePrice && minimumBond) {

            var bondInUsd = parseInt(minimumBond * runePrice);

            minimumBond = utils.numberWithCommas(minimumBond);
            bondInUsd = utils.numberWithCommas(bondInUsd);
        }
        else {
            var bondInUsd = "CONNECTING"
        }

        var asgardActiveVaultLink = await $services.getActiveAsgardVaultLink()
        var totalTx = await $services.getThorTotalTransactions()
        if (totalTx)
            totalTx = utils.numberWithCommas(totalTx)
        else
            totalTx = 0

        return {
            minimumBond,
            bondInUsd,
            asgardActiveVaultLink,
            totalTx
        };
    },

    async fetchRuneViewData($services) {
        var runePrice = await $services.getRunePrice()
        var netValuesMCCN = await $services.getThorNetValuesMCCN()

        var coingecko = await $services.getCoingeckoData()
        var circulatingSupply = parseInt(coingecko.netInfo.circulatingSupply)
        var circulatingSupplyTxt = numberWithCommas(circulatingSupply)

        var MCCN = {
            ...parseRuneNetValues(netValuesMCCN, runePrice)
        }
        MCCN['runeLocked'] = parseNetValuesGetRuneLocked(MCCN.totalCapital, coingecko)
        MCCN['pools'] = netValuesMCCN['pools']
        let runeLocked = parseNetValuesGetRuneLocked(MCCN.totalCapital, coingecko)
        
        return {
            circulatingSupplyTxt,
            runeLocked,
            MCCN
        }
    },

    async fetchNodeOpsPage($services) {
        var minimumBond = await $services.getMinimumBond() ?? 'Loading'
        var runePrice = await $services.getRunePrice()

        if (runePrice && minimumBond) {

            var bondInUsd = parseInt(minimumBond * runePrice);

            minimumBond = utils.numberWithCommas(minimumBond);
            bondInUsd = utils.numberWithCommas(bondInUsd);
        }
        else {
            var bondInUsd = "Loading"
        }

        var MCCN = await $services.getThorNetValuesMCCN()

        return {
            minimumBond,
            bondInUsd,
            MCCN
        };
    }
}

function parseNetValuesGetAPY(netValues) {
    var nodesAPY, lpAPY = null
    if (netValues) {
        nodesAPY = parseInt(netValues.bondingAPY * 100)
        lpAPY = parseInt(netValues.liquidityAPY * 100)
    }
    return { nodesAPY, lpAPY }
}
function parseNetValuesGetStats(netValues, lastBlock, runePrice) {
    var activeNodeCount, standbyNodeCount, nextChurnTime, totalPool, swapVolumeInUSD, tvlInUsd = null
    if (netValues) {
        activeNodeCount = netValues.activeNodeCount
        standbyNodeCount = netValues.standbyNodeCount
        totalPool = netValues.totalPool
        tvlInUsd = utils.curFormatter(netValues.totalTvlInUsd);
        if (runePrice)
            swapVolumeInUSD = utils.curFormatter(parseInt(netValues.swapVolume / 100000000) * runePrice);
        //Next churn height time
        if (lastBlock)
            nextChurnTime = utils.timeFormatter(parseInt(Math.abs(lastBlock - netValues.nextChurnHeight) * 5.5));
    }
    return { activeNodeCount, standbyNodeCount, nextChurnTime, totalPool, swapVolumeInUSD, tvlInUsd }
}
function parseNetValuesGetCapitals(netValues, runePrice) {
    var totalPooledUsd, totalCapital, totalCapitalUsd, totalBondedUsd, volume24h, tx24h, users24h = null
    if (netValues && runePrice) {
        var totalStaked = netValues.totalStaked / 100000000
        totalStaked = parseInt(totalStaked * runePrice) * 2
        totalStaked = utils.numberWithCommas(totalStaked)
        totalPooledUsd = totalStaked

        var totalCapital = parseInt(netValues.totalCapital / 100000000)
        var totalCapitalUSD = parseInt(totalCapital * runePrice)
        var totalCapitalTxt = utils.numberWithCommas(totalCapitalUSD)
        totalCapitalUsd = totalCapitalTxt

        var totalBonded = parseInt(netValues.totalActiveBond / 100000000)
        totalBonded = parseInt(totalBonded * runePrice)
        totalBondedUsd = utils.numberWithCommas(totalBonded)

        var volume = parseInt(netValues.volume24h / 100000000)
        volume = parseInt(volume * runePrice)
        volume24h = utils.nFormatter(volume)
        tx24h = utils.nFormatter(netValues.totalTx24h)
        users24h = utils.nFormatter(netValues.users24h)
    }
    return { totalPooledUsd, totalCapital, totalCapitalUsd, totalBondedUsd, volume24h, tx24h, users24h }
}
function parseNetValuesGetNodesIncome(minimumBond, netValues, runePrice) {
    var nodeIncomeDaily,
        nodeIncomeDailyUSD,
        nodeIncomeYearlyUSD,
        nodeIncomeYearly,
        nodeTweetText,
        nodeTweetDate = null
    if (minimumBond && netValues && runePrice) {

        nodeIncomeYearly = netValues.bondingAPY * minimumBond
        nodeIncomeYearlyUSD = (nodeIncomeYearly * runePrice)
        nodeIncomeDaily = (nodeIncomeYearly / 365)
        nodeIncomeDailyUSD = (nodeIncomeDaily * runePrice)

        nodeIncomeYearly = utils.numberWithCommas(parseInt(nodeIncomeYearly))
        if (nodeIncomeYearlyUSD > 1)
            nodeIncomeYearlyUSD = utils.numberWithCommas(parseInt(nodeIncomeYearlyUSD))
        else
            nodeIncomeYearlyUSD = nodeIncomeYearlyUSD.toFixed(2)

        if (nodeIncomeDaily > 1)
            nodeIncomeDaily = utils.numberWithCommas(parseInt(nodeIncomeDaily))
        else
            nodeIncomeDaily = nodeIncomeDaily.toFixed(2)

        if (nodeIncomeDailyUSD > 1)
            nodeIncomeDailyUSD = utils.numberWithCommas(parseInt(nodeIncomeDailyUSD))
        else
            nodeIncomeDailyUSD = nodeIncomeDailyUSD.toFixed(2)


        nodeTweetText = `My $RUNE node is currently printing me ${nodeIncomeDaily} $RUNE per day, roughly $${nodeIncomeDailyUSD}/day

    That’s $${nodeIncomeYearlyUSD}/year
    Haters mad`
        nodeTweetDate = new Date().toLocaleString('en', { day: 'numeric', month: 'short' })
    }
    else {
        nodeTweetText = `My $RUNE node is currently printing me 860 $RUNE per day, roughly $1,550/day

    That’s $565,000/year
    Haters mad`
        nodeTweetDate = "Jan 13"
    }

    return {
        nodeIncomeDaily,
        nodeIncomeDailyUSD,
        nodeIncomeYearlyUSD,
        nodeTweetText,
        nodeTweetDate,
    }
}
function parseNetValuesGetRuneLocked(totalCapital, coingecko){
    var runeLocked = ""
    if (totalCapital && coingecko && coingecko.netInfo) {
        var circulatingSupply = parseInt(coingecko.netInfo.circulatingSupply)
        runeLocked = (totalCapital / circulatingSupply) * 100
        runeLocked = parseInt(runeLocked)
    }
    return runeLocked
}

function parseRuneNetValues(netValues, runePrice){
    if (runePrice && netValues) {

        var totalStaked = netValues.totalStaked / 100000000
        totalStaked = parseInt(totalStaked * runePrice)
        var nonRuneTVL = totalStaked    
        totalStaked = utils.numberWithCommas(totalStaked)
        var nonRuneTVLTxt = totalStaked

        var totalCapital = parseInt(netValues.totalCapital / 100000000)
        var totalCapitalTxt = utils.numberWithCommas(totalCapital)

        var nodeAPY = parseInt(netValues.bondingAPY * 100)
        var liquidAPY = parseInt(netValues.liquidityAPY * 100)

        var totalEarned = netValues.totalEarned / 100000000
        var totalEarnedUsd = parseInt(totalEarned * runePrice)
        var totalEarnedTxt = utils.numberWithCommas(totalEarnedUsd)

    }
    else {

        var nonRuneTVL = 0
        var totalCapital = 0
        var totalCapitalTxt = "CONNECTING"
        var nodeAPY = 0
        var liquidAPY = 0
        var totalEarnedTxt = "CONNECTING"
    }
    return {
        nonRuneTVL,
        nonRuneTVLTxt,
        totalCapital,
        totalCapitalTxt,
        nodeAPY,
        liquidAPY,
        totalEarnedTxt
    }
}
