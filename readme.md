# Thorchain.org V2

This is the implementation of 2nd version of thorchain.org in nuxtjs with Vue framework.

## Setup
install dependencies:
```sh
yarn
```
## Start
start in development mode:

```sh
yarn dev
```

build and start in SSR mode (server side rendering):
```sh
yarn build
yarn start
```
