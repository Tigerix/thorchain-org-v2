export default function ({route, redirect, $device}) {
    if(!$device.isMobile){
        if(process.env.baseUrl){
            redirect(process.env.baseUrl + route.fullPath)
        }
    }
  }