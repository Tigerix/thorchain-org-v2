export const state = () => ({
    showYoutubeOverlay: true,
    loadingMode: false,
    isStoryCardOpen: false
})

export const mutations = {
    showYoutubeOverlay(state, payload){
        state.showYoutubeOverlay = payload
    },
    loadingMode(state, payload){
        state.loadingMode = payload
    },
    isStoryCardOpen(state, payload){
        state.isStoryCardOpen = payload
    }
}

export const getters = {
    showYoutubeOverlay: state =>{
        return state.showYoutubeOverlay
    },
    loadingMode: state => {
        return state.loadingMode
    },
    isStoryCardOpen: state => {
        return state.isStoryCardOpen
    },
    
}
