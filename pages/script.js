import { fetchHomePageData } from "../utils/commonFunctions";

export default {
  name: 'defaultPage',
  data() {
    return {
      missionHeader: {
        title: "Mission",
      },
      videoContent: {
        text:
          "THORChain aims to decentralize cryptocurrency liquidity via a network of public THORNodes and ecosystem products. Access to its native and cross-chain liquidity is open to any person, product or institution.",
        image: require("~/assets/images/homepage/mission/video-mission.png"),
        link: true,
        isYoutube: true,
        videoId: "VEqHk12eHUA",
      },
      ganttChart: {
        gantts: [
          {
            colStart: 1,
            colSpan: 8,
            rowStart: 1,
            rowSpan: 1,
            background: "linear-gradient(180deg, #2FFAA1 0%, #49F0E6 100%)",
            text: "#ChaosNet",
            title: "• LIVE",
            titleColor: "#2FFAA1",
          },
          {
            colStart: 4,
            colSpan: 5,
            rowStart: 2,
            rowSpan: 1,
            background: "#FFF",
            text: "TERRA Integration",
            title: "• LIVE",
            titleColor: "#2FFAA1",
          },
          {
            colStart: 9,
            colSpan: 4,
            rowStart: 1,
            rowSpan: 2,
            background: "#FFF",
            text: "#MainNet",
            title: "2022",
            extraText:
              "• No admin keys\n• Self-sustained network",
          },
        ],
      },
      singleChart: {
        gantts: [
          {
            colStart: 2,
            colSpan: 13,
            rowStart: 1,
            rowSpan: 1,
            background: "#f3ba2f",
            text: "#StageNet",
            title: "• LIVE",
            titleColor: "#2FFAA1",
          },
          {
            colStart: 3,
            colSpan: 12,
            rowStart: 2,
            rowSpan: 1,
            background: "#FF4954",
            text: "#Attribute BETA",
            title: "• LIVE",
            titleColor: "#2FFAA1",
          },
        ],
      },
      runeTokenHeader: {
        title: "RUNE Token",
      },
      highlights: {
        cards: [
          {
            mode: "text",
            title: "Latest Highlights",
            text:
              "The permissionless nature of the protocol gave birth to a fast-growing ecosystem.\n\nCheckout some of the latest community highlights from the THORNOOBs weekly recap.",
            buttonText: "Read the Weekly Recap",
            class: ['homepage'],
            buttonLink: this.$links('slash_weekly_recap'),
          },
          {
            mode: "highlight",
            text: "THORWallet\nWeb app is Live",
            img: require("/assets/images/highlights/THORWallet-highlight.svg"),
            linkProps: this.$links("highlights_thorchain_dev_update_140-143"),
          },
          {
            mode: "highlight",
            text: "THORChain\nDev Update #140-143",
            img: require("/assets/images/highlights/thorchain-dev.png"),
            linkProps: this.$links("highlights_thorchain_dev_update_140-143"),
          },
          {
            mode: "highlight",
            text: "THORChain\nDev Update #139-140",
            img: require("/assets/images/highlights/thorchain-dev.png"),
            linkProps: this.$links("highlights_thorchain_dev_update_139-140"),
          },
          {
            mode: "highlight",
            text: "Native RUNE\nis listed on Kraken",
            img: require("~/assets/images/highlights/kraken-rune.png"),
            linkProps: this.$links("highlights_kraken_listed"),
          },
          {
            mode: "highlight",
            text: "THORChain\n$500K Bounty Awarded for\nCritical TSS Bug",
            img: require("~/assets/images/highlights/thorchain-immunefi.png"),
            linkProps: this.$links("highlights_thorchain_500k_bug_bounty_awarded_tss_bug"),
          },
        ],
      },
      deterministicValue: {
        title: "Aiming for Deterministic Value",
        text:
          "If over 80% of circulating RUNE gets locked into THORChain liquidity pools, by economic design RUNE’s market cap should be a minimum 3X the value of all non-RUNE assets locked into THORChain liquidity pools (BTC, ETH, BNB, BCH, LTC, DOGE.\n\nThe more RUNE holders provide liquidity with their RUNE, the more accurate deterministic RUNE becomes. Assuming 80% of circulating RUNE is locked into THORChain.\n\n26% of RUNE is locked in THORChain liquidity pools today.\n",
        video: require("~/assets/images/homepage/rune/coin.webm"),
        img: require("~/assets/images/homepage/rune/coin.gif"),
        imgAlt: "3D RUNE coin",
      },
      tileComponent: [
        {
          rowStart: 1,
          rowSpan: 4,
          colStart: 1,
          colSpan: 2,
          title: "Getting Started",
          text:
            "THORChain is a complex technology.\n\nWe recommend to read Erik Voorhees’ summary first, it’s a good introduction to the whole.",
          buttons: [
            {
              text: "Erik’s Summary",
              linkProps: this.$links("getting_started_erik_summary"),
            },
            { 
              text: "Technology",
              linkProps: this.$links("slash_technology"),
            },
            { 
              text: "RUNE Token",
              linkProps: this.$links("slash_rune"),
            },
            { 
              text: "Network",
              linkProps: this.$links("slash_network"),
            },
            { 
              text: "THORFi",
              linkProps: this.$links("slash_thorfi"),
            },
          ],
        },
        {
          rowStart: 1,
          rowSpan: 2,
          colStart: 3,
          colSpan: 4,
          title: "Ecosystem →",
          text:
            "The ecosystem is entirely community-driven. Trusted products and services built by THORChads include swap/pool interfaces, wallets, network explorers, name services, arbitrageur tools and much more.\n\nSupport the ecosystem, THORChain doesn’t exist without it.",
          linkProps: this.$links("slash_ecosystem"),
        },
        {
          rowStart: 3,
          rowSpan: 2,
          colStart: 3,
          colSpan: 2,
          title: "Security →",
          text:
            "The network is fully transparent on its security practices. Solvency can be fully verified on-chain.",
          linkProps: this.$links("slash_security_solvency"),
        },
        {
          rowStart: 3,
          rowSpan: 1,
          colStart: 5,
          colSpan: 2,
          title: "Roles →",
          text: "The community welcomes all.",
          linkProps: this.$links("slash_community_roles"),
        },
        {
          rowStart: 4,
          rowSpan: 1,
          colStart: 5,
          colSpan: 1,
          title: "Library →",
          linkProps: this.$links("slash_document_library"),
        },
        {
          rowStart: 4,
          rowSpan: 1,
          colStart: 6,
          colSpan: 1,
          title: "Research\nLabs →",
          linkProps: this.$links("slash_research_labs"),
        },
      ],
      mainCard: {
        title: "Valhalla Awaits",
        text:
          "Whether you’re looking to swap your native BTC, ETH/ERC20, LTC, BNB, BCH, DOGE between one another, or looking to earn yield on your native assets by providing liquidity to pools — valhalla will keep you amazed.\n\nSupporting the following chains:",
        innerImage: require("~/assets/images/homepage/valhalla/chains.png"),
        mainImage: require("~/assets/images/homepage/valhalla/illustration.png"),
        buttons: [{ text: "Swap Native", linkProps: this.$links('slash_interfaces')}, { text: "Pool Native", linkProps: this.$links('slash_liquidity_providers')}],
      },
      MCCN: {
        activeNodeCount: "...",
        standbyNodeCount: "..."
      }
    };
  },
  methods: {
    maplibre() {
      var map = new maplibregl.Map({
        container: 'map',
        style: 'https://api.maptiler.com/maps/1f9f7964-6750-45fc-98a8-85940f6a269d/style.json?key=Tp6qoZHaRtXnykBQhOeu', // stylesheet location
        center: [0, 0], // starting position [lng, lat]
        zoom: -1, // starting zoom
        interactive: false
      });

      this.nodesLocation?.forEach(el => {
        if (el.lat && el.lon) {
          var markerIcon = document.createElement('div');
          markerIcon.className = 'marker';
          let iconImage = el.stat === 'Active' ? "url(" + require('~/assets/images/icons/active-marker.png') + ")":"url(" + require('~/assets/images/icons/inactive-marker.png') + ")"
          markerIcon.style.backgroundImage = iconImage;
          markerIcon.style.width = '16px';
          markerIcon.style.height = '16px';
          markerIcon.style.backgroundSize = 'cover';
          new maplibregl.Marker(markerIcon, {
            anchor: 'bottom'
          })
            .setLngLat([el.lon, el.lat])
            .addTo(map);
        }
        else
          console.log("undefined result")
      });

    }
  },
  mounted() {
    if (window) {
      this.maplibre();
    }
  },
  async asyncData({app, $services}) {
    let res = await fetchHomePageData($services)
    return {
      ...res,
      //dynamic data
      deterministicValue: {
        title: "Aiming for Deterministic Value",
        text:
          `If over 80% of circulating RUNE gets locked into THORChain liquidity pools, by economic design RUNE’s market cap should be a minimum 3X the value of all non-RUNE assets locked into THORChain liquidity pools (BTC, ETH, BNB, BCH, LTC, DOGE).\n\nThe more RUNE holders provide liquidity with their RUNE, the more accurate deterministic RUNE becomes.\n\n${(res.MCCN && res.MCCN.runeLocked) || '34'}% of RUNE is locked in THORChain liquidity pools today.\n`,
        video: require("~/assets/images/homepage/rune/coin.webm"),
        img: require("~/assets/images/homepage/rune/coin.gif"),
        imgAlt: "3D RUNE coin",
        buttonText: "Learn More About RUNE",
        linkProps: app.$links("slash_rune"),
      },
    }     
  },
  head() {
    return {
      title: "THORChain.org | The Decentralized & Autonomous Cross-Chain Liquidity Network",
      meta: [
        { property: "og:locale", content: "en_US" },
        { property: "og:type", content: "website" },
        { property: "og:title", content: "THORChain.org | The Decentralized & Autonomous Cross-Chain Liquidity Network" },
        {
          property: "og:description",
          content:
            "THORChain aims to decentralize cryptocurrency liquidity via a network of public THORNodes and ecosystem products. Access to its native and cross-chain liquidity is open to any person, product or institution.",
        },
        { property: "og:url", content: process.env.baseUrl },
        { property: "og:site_name", content: "THORChain" },
        {
          property: "og:image",
          content: process.env.baseUrl + "/images/meta-homepage.png",
        },
        { property: "og:image:width", content: "876" },
        { property: "og:image:heigh", content: "438" },
        { name: "twitter:creator", content: "@thorchain" },
        { name: "twitter:site", content: "@thorchain" },
        { name: "twitter:title", content: "THORChain.org | The Decentralized & Autonomous Multi-Chain Liquidity Network" },
        {
          name: "twitter:description",
          content:
            "THORChain aims to decentralize cryptocurrency liquidity via a network of public THORNodes and ecosystem products. Access to its native and cross-chain liquidity is open to any person, product or institution.",
        },
        { name: "twitter:card", content: "summary_large_image" },
        {
          name: "twitter:image",
          content: process.env.baseUrl + "/images/meta-homepage.png",
        },
      ],
      script: [
        {
          src:
            "https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js",
            defer: true,
        },
        { src: "https://unpkg.com/maplibre-gl@2.1.1/dist/maplibre-gl.js", },
      ],
    };
  },
};
