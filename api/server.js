const express = require('express')
const app = express()

const org = require('./rest')
var port = 9999


app.use('/org', org)

app.listen(port, () => {
    console.log(`ORG server listening at http://localhost:${port}`)
})