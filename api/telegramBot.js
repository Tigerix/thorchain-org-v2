
function botFactory(){
    if (!process.env.TELEGRAM_BOT_TOKEN) return {
        log(msg){
            return
        }
    };
    
    const TelegramBot = require('node-telegram-bot-api');
    
    // replace the value below with the Telegram token you receive from @BotFather
    const token = process.env.TELEGRAM_BOT_TOKEN;
    
    // Create a bot that uses 'polling' to fetch new updates
    const bot = new TelegramBot(token, { polling: true });
    
    // Matches "/echo [whatever]"
    bot.onText(/\/start/, (msg, match) => {
        
        const chatId = msg.chat.id;
        const resp = match[1]; // the captured "whatever"
        var message = "Hello " + msg.chat.username
        message += "Your chat id is: "+ chatId
        // send back the matched "whatever" to the chat
        bot.sendMessage(chatId, message);
    });
    
    
    const logReportAuthorizedChats = [
        1116041467
    ]
    
    var botFunctions = {
        log(msg){
            for(var chatId of logReportAuthorizedChats){
                bot.sendMessage(chatId, msg)
            }
        }    
    }
    botFunctions.log("Telegram Bot started")
    return botFunctions
}
module.exports = botFactory()