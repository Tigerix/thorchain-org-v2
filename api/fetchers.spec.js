var assert = require('assert');
const Fetchers = require('./fetchers');
const { fetchThorNetValues } = require('./fetchersV2');
describe('"Midgard"', function() {
    this.timeout(5000);
    // it('fetch Block Height', async function() {
    //     var blockheight  = await Fetchers.fetchLastBlock()
    //     assert.strictEqual("string", typeof(blockheight))
    //     assert.ok(parseInt(blockheight) > 100)
    //     console.log("BlockHeight: " + blockheight)
    // });
    // it('fetch Minimum Bond', async function() {
    //     var minBond  = await Fetchers.fetchMinimumBond()
    //     assert.ok(parseInt(minBond) > 100)
    //     console.log("Minimum Bond: " + minBond)
    // });
    // it('fetch Rune Price', async function() {
    //     var res  = await Fetchers.fetchRunePrice()
    //     console.log("Rune Price: " + res)
    // });
    // it('fetch Asgard Vault Active', async function() {
    //     var res  = await Fetchers.fetchActiveAsgardVaultLink()
    //     console.log("Asgard Vault: " + res)
    // });
    xit('fetch Thorchain Network Values', async function() {
        var res  = await Fetchers.fetchThorNetValues()
        assert.ok(res.activeNodeCount != null)
        assert.ok(res.totalStaked != null)
        assert.ok(res.totalActiveBond != null)
        assert.ok(res.totalStandbyBond != null)
        assert.ok(res.totalReserve != null)
        assert.ok(res.bondingAPY != null)
        assert.ok(res.liquidityAPY != null)
        assert.ok(res.activeNodeCount != null)
        assert.ok(res.standbyNodeCount != null)
        assert.ok(res.totalCapital != null)
        assert.ok(res.totalEarned != null)
        assert.ok(res.totalTx != null)
        assert.ok(res.totalTx24h != null)
        assert.ok(res.volume24h != null)
        assert.ok(res.users24h != null)
        console.log(res)
    });
     
    // it('fetch total transactions', async function() {
    //     var res  = await Fetchers.fetchTotalTXs()
    //     assert.ok(parseInt(res) > 100)
    //     console.log("total txs: " + res)
    // });


    // it('fetch "ViewBlock" total transactions', async function() {
    //     var res  = await Fetchers.fetchViewBlockTotalTx()
    //     assert.ok(parseInt(res) > 100)
    //     console.log("ViewBlock total tx: " + res)
    // });

    it('get net values', async ()=>{
        var tic = Date.now()
        var res = await fetchThorNetValues()
        var toc = Date.now()
        console.log(res)
        console.log(toc-tic)
    })
});