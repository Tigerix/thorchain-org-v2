const express = require('express')
const app = express()
const fetchers = require('./fetchers');
const fetchersV2 = require('./fetchersV2');

const axios = require('axios');
const e = require('express');
const { reportError } = require('./errorHandling');
const utils = require('../utils');
const telegramBot = require('./telegramBot');
const cors = require('cors')

const corsOptions = {
  origin: process.env.MOBILE_BASE_URL
}
app.use(express.json());
app.use(cors(corsOptions))

//should retiring the old midgard api. still it can be used in lagacy api in midgard V2
function createExpressApp() {
  const SERVER_START_TIME = Date.now()
  telegramBot.log("Starting the server")
  
  var reqCount = 0
  var data = {
    'coingeckoMarkets':{ fetcher: fetchersV2.fetchCoingeckoMarkets, updateEvery: 5 /*seconds*/ },
    'coingeckoAllMarkets':{ fetcher: fetchersV2.fetchCoingecko_All_Markets, updateEvery: 20 },
    'coingeckoNetInfo':{ fetcher: fetchersV2.fetchCoingeckoNetInfo, updateEvery: 5 },
    'runePrice':{ fetcher: fetchersV2.fetchRunePrice, updateEvery: 5 },
    'minBond':{ fetcher: fetchersV2.fetchMinimumBond, updateEvery: 5 },
    'lastBlock':{ fetcher: fetchersV2.fetchLastBlock, updateEvery: 1 },
    'thorNetValuesMCCN':{ fetcher: fetchersV2.fetchThorNetValues, updateEvery: 1},
    'coingecko_ERC20_Markets': {fetcher: fetchers.fetchCoingecko_ERC20_Markets, updateEvery: 5},
    'nodesLocation': {fetcher: fetchersV2.fetchNodesLocation, updateEvery: 120}
  }

  /* Update all the values at server init */
  setTimeout(async () => {
    for (var key of Object.keys(data)) {
      (() => {
        var currentKey = key
        var record = data[key]
        record['lastUpdate'] = Date.now()

        record.fetcher().then((res)=>{
            record['value'] = res
            record['err'] = null
        })
        .catch(rej =>{
          record['value'] = null
          record['err'] = rej
        })
      })()
    }
  }, 0);

  setInterval(async () => {
    for (var key of Object.keys(data)) {
      var record = data[key]

      /* update the record if it's the time */
      if (Date.now() - record.lastUpdate >= record.updateEvery * 1000) {

        (() => {
          var currentKey = key
          var record = data[key]
          record['lastUpdate'] = Date.now()
  
          record.fetcher().then((res)=>{
            if(res)
              record['value'] = res
              record['err'] = null
          })
          .catch(rej =>{
            record['value'] = null
            record['err'] = rej
            console.error(currentKey +": failed")
            console.error(rej)
          })
        })()

      }
    }
  }, 500);

  app.get('/api/static_data/:key', async (req, res)=>{      
    try{
      console.log(`Server: ${req.url} called`)
      var key = req.params.key
      if(key in data) {
        
        if(!data[key].value){
          // telegramBot.log(`No values for '${key}'. Server is running for ${utils.timeConversion(Date.now() - SERVER_START_TIME)}`)
          // Adding this to make sure there is backend data for us
          try {
            console.log('getting data from backend server');
            var serverBackupData = await axios.get('https://api.attribute.me/org/api/static_data/' + req.params.key)
            res.setHeader('Cache-Control', 's-maxage=60, stale-while-revalidate=119')
            if (serverBackupData) {
              data[key].value = serverBackupData.data;
              return res.json(serverBackupData.data);
            }
            else
              throw new Error('Can\'t get the data from backend server!');
          } catch (e) {
            return res.status(404).json({msg: "external api not responding", key})
          }
        }
        
        var value = data[key].value
        res.setHeader('Cache-Control', 's-maxage=60, stale-while-revalidate=119')
        res.json(value)
      }
      else{
        res.status(404).json({msg: 'Static data Not found', key})
      }
    }
    catch(e){
      console.error(e)
    }
  })

  app.get('/api/coingecko', (req, res) => {
    try {
      var coingecko = {
        markets: data.coingeckoMarkets? data.coingeckoMarkets.value: {},
        netInfo: data.coingeckoNetInfo? data.coingeckoNetInfo.value: {},
        lastUpdate: data.coingeckoMarkets? data.coingeckoMarkets.lastUpdate: 0
      }
      res.json(coingecko)
    }
    catch (e) {
      console.error(e)
    }
  })
  app.get('/api/rc', (req,res)=>{
    reqCount++
    res.json(reqCount)
  })
  app.get('/api/*', (req, res)=>{
    res.status(404).send({msg: 'Not found', url: req.url})
  })
  return app
}

module.exports = createExpressApp()
